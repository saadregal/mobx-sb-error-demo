import React from 'react';
import cartStore from './store'
import {observer} from "mobx-react";
const CartComponent = () => {
    return (
        <div>
            number of cart item : {cartStore.cart.length} 
        </div>
    );
};

export default observer(CartComponent);


