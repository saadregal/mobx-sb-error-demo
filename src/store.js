import {observable, action} from 'mobx';


class RouterStore {
    @observable cart = [];

    @action addTocart(item) {
        this.cart.push(item)
    }
}

const cartStore = new RouterStore();
export default cartStore;