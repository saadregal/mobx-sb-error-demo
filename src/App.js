import React from 'react';
import Cart from './CartComponent'
import store from './store';
function App() {
  const items= ["banana", "apple", "bread", "egg"]
  return (
   <>
    <Cart/>
    {items.map((item)=>
      <button onClick={()=>store.addTocart(item)}>
          {item}
      </button>
    )}
   </>
  );
}

export default App;
